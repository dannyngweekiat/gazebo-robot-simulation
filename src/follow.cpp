#include "ros/ros.h"
#include "tf2_ros/transform_listener.h"
#include "tf2/LinearMath/Quaternion.h"
#include "tf2/LinearMath/Matrix3x3.h"

#include "sensor_msgs/LaserScan.h"
#include "geometry_msgs/Twist.h"

ros::Publisher velocityPublisher;

void onSensor(sensor_msgs::LaserScan scan)
{
    int i = 0;

    int minContinousScan = 5;
    int continousCount = 0;
    bool matched = false;

    double matchedAngle = 0;
    double matchedDistance = 0;

    for (double t = scan.angle_min; t <= scan.angle_max; t += scan.angle_increment, i++)
    {
        // Simple blob detection (object below 2m) - Only take 1st blob
        if (!isinf(scan.ranges[i]))
        {
            if(!matched){
                if(scan.ranges[i] < 3){
                    if(continousCount == minContinousScan){
                        matched = true;
                        matchedAngle = t - 2*scan.angle_increment;
                        matchedDistance = scan.ranges[i-2];
                    }
                    continousCount++;
                }
                else{
                    continousCount = 0;
                }
            }
        }
    }
    if(matched){
         ROS_INFO("%f %f", matchedAngle, matchedDistance);
    }else{
         ROS_INFO("No match");
    }
    // Drive robot here using simple p controller
    geometry_msgs::Twist msg;
    double diff = matchedDistance - 0.75;
    msg.linear.x = diff > 0 ? diff * 0.25 : 0;
    msg.angular.z = matchedAngle * 0.5;
    velocityPublisher.publish(msg);
}
int main(int argc, char **argv)
{
    ros::init(argc, argv, "follow");
    ros::NodeHandle n;
    ros::NodeHandle nh("~");

    std::string sub, pub;

    nh.param<std::string>("incoming_scan", sub, "/scan");
    velocityPublisher = n.advertise<geometry_msgs::Twist>("/diff_drive_controller/cmd_vel", 50);

    ros::Subscriber vel_sub = n.subscribe(sub, 50, onSensor);
    ros::spin();
    return 0;
}
