cmake_minimum_required(VERSION 2.8.3)
project(gazebo_robot_simulation)

add_compile_options(-std=c++11)

# Load catkin and all dependencies required for this package
find_package(catkin REQUIRED COMPONENTS 
  roscpp 
  gazebo_ros 
  sensor_msgs
  geometry_msgs
)

# Depend on system install of Gazebo
find_package(gazebo REQUIRED)

link_directories(${GAZEBO_LIBRARY_DIRS})
include_directories(${Boost_INCLUDE_DIR} ${catkin_INCLUDE_DIRS} ${GAZEBO_INCLUDE_DIRS})

catkin_package(
  DEPENDS 
    roscpp 
    gazebo_ros 
)

add_executable(gazebo_robot_simulation src/follow.cpp)

target_link_libraries(gazebo_robot_simulation ${catkin_LIBRARIES})
